import 'dart:io';

void main(List<String> arguments) {
  print('Сколько чисел желаете ввести?');
  var sequinceOfList = readln();
  var garbageOfInputs = <int>[];
  if (sequinceOfList == '=') {
    print('Завершение программы $garbageOfInputs');
    exit(0);
  } else if (sequinceOfList == null) {
    print('Не удалось добавить значение, так как это не число.');
    sequinceOfList = readln();
  }
  print('Введите число');
  for (int i = 1; i <= sequinceOfList; i++) {
    var toggle = 0;
    while (toggle == 0) {
      var valueOfInput = readln();

      if (valueOfInput == '=') {
        print('Завершение программы $garbageOfInputs');
        exit(0);
      } else if (valueOfInput == null || valueOfInput == '') {
        print(
            'Не удалось добавить значение, так как это не число. Список: $garbageOfInputs');
      } else {
        garbageOfInputs.add(valueOfInput);
        print('Значение успешно добавлено. Список: $garbageOfInputs');
        toggle = 1;
      }
    }
  }
  print((checkClones(garbageOfInputs)).join('\n'));
}

readln() {
  var input = stdin.readLineSync()!;
  if (input == '=') return '=';
  return int.tryParse(input);
}

Set<String> checkClones(List inputList) {
  var setOfInputList = inputList.toSet().toList();
  int counter = 0;
  List<int> aqumulation = [];

  for (var uniqueElement in setOfInputList) {
    /** цикл записывает неуникальные значения */
    for (var iteratingElement in inputList) {
      if (uniqueElement == iteratingElement) {
        aqumulation.add(iteratingElement);
      }
    }
  }

  List<int> setOfaqumulation = aqumulation.toSet().toList();
  List<int> changingList = aqumulation;
  /** копия листа, чтобы изменнять его во время итерации, т.к. изменение итерируемого листа плохая идея */
  for (var el in setOfaqumulation) {
    if (changingList.contains(el)) changingList.remove(el);
  }

  List<String> resultList = [];

  List<String> aqumulationOfvalues = [];
  int b = 0;
  for (var element1 in setOfInputList) {
    /** цикл аккумулирует значения и индексы ВСЕХ элементов */
    counter = 0;
    int a = 0;
    for (var element2 in inputList) {
      if (element1 == element2) {
        if (a == 0) {
          aqumulationOfvalues
              .add("$element2: ${inputList.indexOf(element2, counter)}");
          counter = (inputList.indexOf(element2, counter)) + 1;
          a = 1;
        } else {
          aqumulationOfvalues[b] =
              ("${aqumulationOfvalues[b]}${inputList.indexOf(element2, counter)}");
          counter = (inputList.indexOf(element2, counter)) + 1;
        }
      }
    }
    a = 0;
    b += 1;
  }

  for (var el in changingList) {
    /** цикл фильтрует aqumulationOfvalues от уникальных значений (оставляет только повторяющиеся более 1 раза) */
    for (var el2 in aqumulationOfvalues) {
      if (el == int.parse(el2.substring(0, 1))) {
        resultList.add(el2);
      }
    }
  }

  return resultList.toSet();
}
